# SaIB

a [Sails](https://sailsjs.com)  Image Board


### Links

+ [Sails framework documentation](https://sailsjs.com/documentation)
+ [Vue framework documentation](https://vuejs.org/)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/studio)
