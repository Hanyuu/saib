/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */
const bcrypt = require('bcrypt');
module.exports.bootstrap = async function(done) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  let defaultAdmin = {
    login:'Hanyuu',
    password:'hauau'
  };
  let user = await User.findOne({login:defaultAdmin.login});
  if(!user){
    await User.create({
      login:defaultAdmin.login,
      sid:' ',
      is_admin:true,
      password:bcrypt.hashSync(defaultAdmin.password,5)
    });
    console.warn('Created default admin, login: ',defaultAdmin.login,' password: ',defaultAdmin.password);
  }
  return done();

};
