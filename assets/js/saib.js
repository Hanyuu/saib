Vue.component('boards-list', {
  template: '#board-list-template',
  computed: {
    boards () {
      return this.$store.state.boards;
    }
  },
  watch: {
    '$route' (to, from) {
      let oldThreadId = from.params.threadId;
      if(oldThreadId){
        io.socket.get('/'+oldThreadId+'/leave/', (leave) => {
          console.log(leave);
        });
      }
      if(!to.params.threadId) {
        for(let board of store.state.boards){
          if (board.name === to.params.boardName){
            io.socket.get('/board/'+board.id+'/threads/'+to.params.page+'/',(body) => {
              this.$store.commit('SET_BOARD',board);
              this.$store.commit('SET_THREADS',body);
            });
          }
        }
      }
    }
  },
  methods:{
  }
});

Vue.component('post', {
  props:['post','posts'],
  template:'#post-template',
  methods: {
    deletePost: function (id) {
      io.socket.delete('/post/' + id, (body) => {
        console.log(body);
      });
    }
  }
});

Vue.component('attachment', {
  props:['file'],
  template:'#attachment-template',
  data:function(){
    return {
      thumb: '?thumb=true'
    };
  },
  computed:{
    fileLink () {
      return '/download/'+this.file.id+'/'+this.thumb;
    }
  },
  methods: {
    showAttachment: function () {
      if (this.file.type.includes('image')) {
        this.fileLink.includes('thumb') ? this.thumb = '' : this.thumb = '?thumb=true';
      }
    }
  }
});

Vue.component('reply-form', {
  props:['threadId','board'],
  template: '#form-template',
  data:function () {
    return {
      toManyFiles:false,
      uploadedFiles:[],
      needCaptcha:true,
      error:false,
      errorMessage:'',
      random:Math.random(),
      captchaLeft:'',
      message:'',
      name:'',
      title:'',
      sage:false
    };
  },
  computed: {
    create(){
      return this.threadId === 0;
    }
  },
  watch:{

  },
  mounted:function(){
    //Hide captcha after post timeout ended.
    setInterval(()=>{
      io.socket.get('/'+this.board.name+'/needcaptcha/',(body)=> {
        this.needCaptcha = body.needCaptcha;
        if (typeof body.captchaLeft === 'string'){
          this.captchaLeft = 'Для отправки сообщения введите капчу.';
        }else {
          let seconds = (Math.round(this.board.post_delay / 1000) - body.captchaLeft);
          this.captchaLeft = 'Вы сможете отправить сообщение не вводя капчу через ' + seconds  + 'c.';
        }
      });
    },1000);
  },
  methods:{
    sendMessage:function () {
      io.socket.put('/'+this.board.name+'/'+this.threadId+'/post/create/',
        {
          title:this.title,
          message:this.message,
          name:this.name,
          sage:this.sage
        },
        (body) => {
          if(body.status === 'error'){
            this.error = true;
            console.log(body);
            switch (body.type) {
              case 'not-found':
                this.errorMessage = 'Не найдена доска';
                break;
              case 'no-message-or-files':
                this.errorMessage = 'Необходимо прикрепить файл или написать сообщение';
                break;
              case 'need-captcha':
                this.random = Math.random();
                this.needCaptcha = true;
                this.errorMessage = 'Необходимо ввести капчу';
                break;
              case 'wrong-captcha':
                this.errorMessage = 'Не правельно введена капча';
                this.random = Math.random();
                break;
            }
          }else{
            //Reset form
            this.error = false;
            this.uploadedFiles = [];
            this.message = '';
            this.name = '';
            this.title = '';
            this.sage = false;
            document.getElementById('file').value ='';
            if(body.status === 'created'){
              router.push('/'+this.board.name+'/thread/'+body.type);
            }
          }
        });
    },
    updateCaptcha:function(){
      this.random = Math.random();
    },
    deleteFile:function(id){
      io.socket.delete('/uploadedfile/'+id+'/', function gotResponse(body, response) {
        console.log(body);
        console.log(response);
      });
    },
    mounted:function(){
    },
    upload:function(){
      let files = document.getElementById('file').files;
      if(files.length > this.board.max_files){
        this.toManyFiles = true;
      }else {
        this.toManyFiles = false;
        let form = new FormData();
        for (let file of files) {
          form.append('file[]', file, file.name);
        }
        $.ajax({
          type: 'POST',
          url: '/' + this.board.name + '/upload/',
          xhr: function () {
            let myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
              myXhr.upload.addEventListener('progress', (evt)=>{
                let loaded = evt.loaded || evt.position;
                console.log(loaded / evt.total);
              }, false);
            }
            return myXhr;
          },
          success:(data)=> {
            if(data.error){
              this.toManyFiles = true;
            }else{
              this.uploadedFiles = data;
            }
          },
          error: function (error) {
            console.log('error');
            console.log(error);
          },
          data: form,
          contentType: false,
          processData: false,
        });
      }
    }
  },
});

let threadsListComponent = Vue.component('threads-list', {
  props:['boardName','page'],
  template: '#thread-list-template',
  computed: {
    threads () {
      return this.$store.state.threads;
    }
  },
  mounted:function(){
    if(this.$store.state.boards.length === 0){
      io.socket.get('/board',(body)=> {
        this.$store.commit('SET_BOARDS',body);
        for(let board of this.$store.state.boards){
          if (board.name === this.boardName){
            this.$store.commit('SET_BOARD',board);
            io.socket.get('/board/'+board.id+'/threads/'+this.page+'/',(body) => {
              this.$store.commit('SET_THREADS',body);
            });
          }
        }
      });
    }
  },
  methods:{

  },
});

let threadComponent = Vue.component('thread', {
  props:['boardName','threadId'],
  template: '#thread-template',
  computed: {
    thread () {
      return this.$store.state.thread;
    }
  },
  mounted:function(){
    if(store.state.boards.length === 0) {
      io.socket.get('/board',(body) => {
        this.$store.commit('SET_BOARDS',body);
      });
    }
    io.socket.get('/thread/'+this.threadId,(body) => {
      this.$store.commit('SET_THREAD',body);
    });
    if(this.threadId > 0) {

      io.socket.get('/' + this.threadId + '/enter/', (joined) => {
        console.log(joined);

        if (!io.socket['joinedTo' + this.threadId]) {

          io.socket['joinedTo' + this.threadId] = true;

          io.socket.on('added-post-'+this.threadId, (post) => {
            console.log(post);
            this.thread.posts.push(post);
          });

          io.socket.on('delete-post-'+this.threadId, (post) => {
            console.log(post);
            let el = document.getElementById(post.id);
            el.parentNode.removeChild(el);
          });

          io.socket.on('thread-counter-'+this.threadId, (count) => {
            console.log(count);
          });
        }

      });
    }
  },
  methods:{
  },
});

const store = new Vuex.Store({
  state:{
    boards: [],
    threads: [],
    thread:void 0,
    board:void 0
  },
  mutations: {
    SET_THREADS (state,newThreads) {
      console.debug('MUTATION SET_THREADS',newThreads);
      state.threads = newThreads;
    },
    SET_BOARDS (state,newBoards) {
      console.debug('MUTATION SET_BOARDS',newBoards);
      state.boards = newBoards;
    },
    SET_THREAD (state,thread) {
      console.debug('MUTATION SET_THREAD',thread);
      state.thread = thread;
    },
    SET_BOARD (state,board) {
      console.debug('MUTATION SET_BOARD',board);
      state.board = board;
    }
  }
});

const router = new VueRouter({
  routes:[
    { path: '/:boardName/', redirect: '/:boardName/1/' },
    { path: '/:boardName/:page/', component: threadsListComponent, props: true },
    { path: '/:boardName/thread/:threadId/', component: threadComponent, props: true }
  ]
});

new Vue({
  el:"#saib",
  store,
  router,
  mounted:function(){
    if(!this.$route.params.boardName) {
      io.socket.get('/board',(body) => {
        this.$store.commit('SET_BOARDS',body);
      });
    }
  },
  methods:{

  },
});
