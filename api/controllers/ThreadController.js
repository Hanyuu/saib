/**
 * ThreadController
 *
 * @description :: Server-side logic for managing threads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var COUNTER = {};
module.exports = {

  findOne:async function(req,res){
    let thread = await Thread.findOne({id:req.params.id});
    let board = await Board.findOne({id:thread.board});
    let posts = await Post.find({thread:thread.id}).populate('files');
    let response = {
      thread:thread,
      board:board,
      posts:posts
    };
    return  res.json(response);
  },

  enter:async function(req,res){
    let thread = req.params.thread;

    if (!COUNTER[thread]){
      COUNTER[thread] = 1;
    }else{
      COUNTER[thread] += 1;
    }
    sails.sockets.blast('thread-counter-'+thread,{counter:COUNTER[thread]});
    return res.json({'status':'join','thread':thread});

  },

  leave:async function(req,res){

    let thread = req.params.thread;

    if (!COUNTER[thread]){
      COUNTER[thread] = 1;
    }else{
      COUNTER[thread] -= 1;
    }

    sails.sockets.leave(req, thread);
    sails.sockets.blast('thread-counter-'+thread,{counter:COUNTER[thread]});

    return res.json({'status':'leave','thread':thread});
  }

};

