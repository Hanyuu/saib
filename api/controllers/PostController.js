/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {
  create:async function(req,res){
    let user = await User.findOne({id:req.session.user.id});
    let threadId = parseInt(req.params.thread);
    let files = await UploadedFile.find({creator:req.session.user.id,post:null});
    let board = await Board.findOne({name:req.params.board});
    let now = new Date();
    let captchaParams = {user:user.id,board:board.id,thread:threadId};
    let sage = req.body.sage;
    let isOp = threadId === 0;
    let message = req.body.message;

    console.log(threadId);


    if(!user){
      return res.json({status:'error',type:'need-captcha'});
    }

    let needCaptcha = user.last_post ? ((now.getTime() - new Date(user.last_post)) < board.post_delay) : true;



    switch(true){
      case files.length === 0 && !req.body.message:
        return res.json({status:'error',type: 'no-message-or-files'});
      case needCaptcha && !req.body.captcha: //send captcha
        return res.json({status:'error',type:'need-captcha'});
      case !board || !user:
        return res.status(404).send({status:'error',type: 'not-found'});
    }

    if(needCaptcha && req.body.captcha){ //check captcha
      let captcha = await Captcha.findOne(captchaParams);
      if(captcha){
        if(req.body.captcha && req.body.captcha === captcha.text){
          await Captcha.destroy(captchaParams);
        }else{
          return res.json({status:'error',type:'wrong-captcha'});
        }
      }else if(needCaptcha && !req.body.captcha){
        return res.json({status:'error',type:'need-captcha'});
      }

    }


    let thread = await Thread.findOrCreate({board: board.id, creator: user.id,id: threadId},{board: board.id, creator: user.id});

    await Post.create({
      creator: req.session.user.id,
      thread: thread.id,
      message: message,
      title: req.body.title,
      name: req.body.name,
      is_op: isOp,
      sage:sage,
      board:board.id
    });
    let post = await Post.find({where:{creator: req.session.user.id, thread: thread.id},sort: 'createdAt DESC'});
    if(!sage){
      await Thread.update({id:thread.id},{updatedAt:now.getTime()});
    }
    post = post[0];
    await UploadedFile.update({creator:req.session.user.id,post:null},{post:post.id});
    await User.update({id:user.id},{last_post:now});
    post['files'] = files;
    if(req.body.message.length > 0){
      req.session.lastPostText = req.body.message;
    }
    if(isOp){
      return res.json({status:'created',type:thread.id});
    }else {
      sails.sockets.blast('added-post-'+threadId, post);
      return res.json({status:'no-error'});
    }
  },
  destroy:async function(req,res){
    let id = req.params.id;
    try {
      let post = await Post.findOne({id: id,creator:req.session.user.id});
      await Post.destroy({id: id,creator:req.session.user.id});
      sails.sockets.blast('delete-post-'+post.thread,{id:id});
    }catch (e){
      return res.json({status:'error',type:'cannot-delete'});
    }
  }
};

