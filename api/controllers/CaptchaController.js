/**
 * CaptchaController
 *
 * @description :: Server-side logic for managing captchas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let svgCaptcha = require('svg-captcha');
module.exports = {
  captcha:async function(req,res){
    let user = await User.findOne({id:req.session.user.id});
    let board = await Board.findOne({name:req.params.board});
    let needCaptcha = user.last_post ? ((new Date() - new Date(user.last_post)) < board.post_delay) : true;

    res.set('Content-Type', 'image/svg+xml');

    if(needCaptcha){
      let captcha = svgCaptcha.create();
      let params = {board:board.id,thread:req.params.thread,user:req.session.user.id};
      let captchaEntity = await Captcha.findOrCreate(params,params);
      let text = captcha.text.toLowerCase();
      await Captcha.update({id:captchaEntity.id},{text:text});
      return res.send(captcha.data);
    }else{
      return res.send('');
    }
  },
  needCaptcha:async function (req,res) {
    let user = await User.findOne({id:req.session.user.id});
    let board = await Board.findOne({name:req.params.board});
    let seconds = new Date() - new Date(user.last_post);
    let needCaptcha = user.last_post ? (seconds < board.post_delay) : true;
    return res.json({needCaptcha:needCaptcha,captchaLeft:user.last_post ? Math.round(seconds / 1000) : ''});
  }
};

