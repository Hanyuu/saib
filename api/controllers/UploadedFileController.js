/**
 * FileController
 *
 * @description :: Server-side logic for managing Files
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let fs = require('fs');
module.exports = {
  upload: async function(req,res){
    let board = await Board.findOne({name:req.params.board});
    let user = req.session.user;

    res.setTimeout(0);

    await UploadedFile.destroy({creator:req.session.user.id,post:null});
    req.file('file[]').upload({
      maxBytes: 50000000
    }, async function whenDone(err, uploadedFiles) {
      if (err){
        return res.json({error:'error-uploading',status:err});
      }else {
        if (uploadedFiles.length > board.max_files) {
          for (let file of uploadedFiles) {
            fs.unlinkSync(file.fd);
          }
          return res.json({'error': 'max-files'});
        } else {
          for (let file of uploadedFiles) {
            await UploadedFile.create({
              path: file.fd,
              name: file.filename,
              type: file.type,
              creator: user.id
            });
          }
          let response = await UploadedFile.find({creator:req.session.user.id,post:null});
          return res.json(response);
        }
      }
    });
  },
  download: async function(req,res){

    let file = await UploadedFile.findOne({id:req.params.id});

    if(file){
      if(req.query['thumb'] === 'true' && file.thumb) {
        return res.attachment(file.path).send(fs.readFileSync(file.thumb));
      }else{
        return res.attachment(file.path).send(fs.readFileSync(file.path));
      }
    }else{
      return res.notFound();
    }
  },
  filesByPost: async function(req,res){
    let files = await UploadedFile.find({post:req.params.id});
    return res.json(files);
  },
  destroy: async function(req,res){
    try{
      await UploadedFile.destroy({id:req.params.id});
      return res.json({deleted:true});
    }catch (e){
      return res.json({deleted:false});
    }
  }
};

