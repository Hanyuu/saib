/**
 * BoardController
 *
 * @description :: Server-side logic for managing boards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const bcrypt = require('bcrypt');
module.exports = {
  threadsPreviews:async function(req,res){
    let id = req.params.id;
    let page = req.params.page;
    let threads = await Thread.find({board:id}).paginate(page - 1,10).sort('updatedAt DESC');
    let board = await Board.findOne({id:id});
    let response = {board:board,threads:[]};
    if(threads.length > 0) {
      for (let thread of threads) {
        let posts = await Post.find({thread: thread.id}).populate('files');
        if (posts.length > 0) {
          let preview = {thread, posts: []};
          preview.posts.push(posts[0]);
          for (let i of [3, 2, 1]) {
            let post = posts[posts.length - i];
            if (post && post.id !== posts[0].id) {
              preview.posts.push(post);
            }
          }
          response.threads.push(preview);
        }
      }
      return res.json(response);
    }else{
      return res.json(response);
    }
  },
  adminLogin: async function(req,res){
    let login = req.body.login;
    let session = req.session;
    let password = req.body.password;
    let user = await User.findOne({login:login});
    if(user) {
      if (bcrypt.compareSync(password, user.password) && user.is_admin){
        await User.update({id: user.id}, {sid: session.id});
        session.user = user;
        return res.view('admin/index');
      }else{
        return res.notFound();
      }
    }else{
      return res.notFound();
    }
  },
  boardsList:async function(req,res){
    let boards = await Board.find();
    return res.view('admin/boards', {
      boards: boards
    });
  },
  usersList:async function(req,res){

  },
  postsList:async function(req,res){

  }

};

