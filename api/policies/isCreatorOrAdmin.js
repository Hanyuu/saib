/**
 * isCreatorOrAdmin
 *
 * @module      :: Policy
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = async function(req, res, next) {
  let user = req.session.user;
  if (user) {
    let model = sails.models[req.options.model];
    let id = req.params.id;
    if(model && id) {
      id = parseInt(id);
      model = await model.findOne({id: id});
      if (model && model.creator) {
        if (user.id === model.creator || user.is_admin) {
          return next();
        }
      }
    }else{
      return next();
    }
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  return res.forbidden('You are not permitted to perform this action.');
};
