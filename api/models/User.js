/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    sid:{
      type :'string',
      unique: true,
      required: true
    },
    login:{
      type:'string',
      unique: true,
      defaultsTo:''
    },
    password:{
      type:'string',
      defaultsTo:''
    },
    last_post:{
      type: 'number',
      autoUpdatedAt: true
    },
    threads:{
      collection:'thread',
      via:'creator'
    },
    posts:{
      collection:'post',
      via:'creator'
    },
    captcha:{
      type: 'string'
    },
    is_admin:{
      type:'boolean',
      defaultsTo:false
    }
  }
};

