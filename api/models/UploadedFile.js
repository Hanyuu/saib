/**
 * File.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const easyimg = require('easyimage');
const fs = require('fs');
module.exports = {

  attributes: {
    path:{
      type:'string',
      required: true
    },
    thumb:{
      type:'string'
    },
    name:{
      type:'string',
      required: true
    },
    type:{
      type:'string',
      required:true
    },
    post:{
      model:'post'
    },
    creator:{
      model:'user',
      required:true
    }
  },
  customToJSON: function() {
    let file = this;
    delete file.creator;
    delete file.path;
    delete file.thumb;
    return file;
  },
  beforeCreate:async function(values, cb){
    if(values.type.includes('image')){
      let thumb = values.path + '-thumb';
      await easyimg.resize({
        src: values.path,
        dst: thumb,
        width: 100,
        height: 100
      });
      values.thumb = thumb;
      return cb();
    }else{
      return cb();
    }
  },
  beforeDestroy: async function(destroyedRecords, cb){
    for(let file of await UploadedFile.find(destroyedRecords)){
      try {
        if (fs.existsSync(file.path)) {
          fs.unlinkSync(file.path);
        }
        if (fs.existsSync(file.thumb)) {
          fs.unlinkSync(file.thumb);
        }
      }catch(err){
        console.error(err);
        return cb(err);
      }
    }
    return cb();
  }
};

