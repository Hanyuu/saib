/**
 * Board.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:'string',
      unique: true,
      required: true
    },
    full_name:{
      type:'string',
      unique: true,
      required: true
    },
    max_files:{
      type:'number',
      defaultsTo:4,
    },
    post_delay:{
      type:'number',
      defaultsTo:60 * 1000, //1 min
    },
    max_threads:{
      type:'number',
      defaultsTo: 50,
    },
    threads:{
      collection:'thread',
      via:'board'
    }
  }
};

