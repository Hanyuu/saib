/**
 * Captcha.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    board:{
      type:'number',
      required:true
    },
    thread:{
      type:'number',
      required:true
    },
    text:{
      type:'string'
    },
    user:{
      type:'number',
      required:true
    }
  }
};

