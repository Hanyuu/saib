/**
 * Thread.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    posts:{
      collection:'post',
      via:'thread'
    },
    pinned:{
      type:'boolean',
      defaultsTo: false,
    },
    max_posts:{
      type:'number',
      defaultsTo: 100,
    },
    creator:{
      model:'user',
      required: true
    },
    board:{
      model:'board',
      required: true
    },
  },
  customToJSON: function() {
    let thread = this;
    delete thread.creator;
    return thread;
  }

};

