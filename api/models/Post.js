/**
 * Post.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes:{
    sage:{
      type:'boolean',
      defaultsTo:false
    },
    is_op:{
      type:'boolean',
      defaultsTo:false
    },
    name:{
      type:'string'
    },
    title:{
      type:'string'
    },
    message:{
      type:'string'
    },
    files:{
      collection:'uploadedfile',
      via:'post'
    },
    thread:{
      model:'thread',
      required: true
    },
    board:{
      model:'board',
      required: true
    },
    creator:{
      model:'user',
      required: true
    },
  },
  customToJSON:function() {
    let post = this;
    delete post.creator;
    return post;
  },
  beforeCreate:async function(values, cb) {
    let message = values.message;
    message = message
      .replaceAll('<','&lt;')
      .replaceAll('>','&gt;;')
      .replaceAll('\n','<br/>')
      .replaceAll('[i]','<i>')
      .replaceAll('[/i]','</i>')
      .replaceAll('[s]','<s>')
      .replaceAll('[/s]','</s>')
      .replaceAll('[b]','<b>')
      .replaceAll('[/b]','</b>');
    values.message = message;
    return cb();
  }
};

